const News = require("../models/news.model");
const { Promotion } = require("../models");
const User = require("../models/user.model");

const existEmail = async (email = '') => {
    const existEmail = await User.findOne({email});
    if (existEmail){
        throw new Error(`El correo ${email} ya esta registrado en la base da datos`);
    }
}

const existUserForId = async (id) => {
    const existUser = await User.findById(id);
    if (!existUser){
        throw new Error(`El id ${id} no representa a un usuario  en la base da datos`);
    }
}

const allowedCollections = (collection = '', collections = []) => {
    const included = collections.includes(collection);
    if(!included){
        throw new Error(`La collección  ${collection} no esta permita, colecciones validas ${collections} `)
    }
    return true;
}

const existNewsForId = async (id) => {
    const existNews = await News.findById(id);
    if (!existNews){
        throw new Error(`El id ${id} no representa a una noticia  en la base da datos`);
    }
}

const existNewsForSlug = async (slug) => {
    const existNews = await News.findOne({slug});
    if (!existNews){
        throw new Error(`El slug ${slug} no representa a una noticia  en la base da datos`);
    }
}

const existNewsSlug = async (slug) => {
    const existNews = await News.findOne({slug});
    if (existNews){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el titulo de la noticia`);
    }
}

const existPromotionForId = async (id) => {
    const existPromotion = await Promotion.findById(id);
    if (!existPromotion){
        throw new Error(`El id ${id} no representa a una promoción  en la base da datos`);
    }
}


module.exports = {
    existEmail,
    existUserForId,
    existNewsForSlug,
    existNewsForId,
    existNewsSlug,
    existPromotionForId,
    allowedCollections
}
