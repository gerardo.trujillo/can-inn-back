const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT, fileValidate} = require('../middlewares');
const { existPromotionForId } = require("../helpers/db-validators");
const { promotionGetPublic,
    promotionGet,
    promotionPost,
    promotionShowPublic,
    promotionPut,
    promotionShow,
    promotionActive,
    promotionDesActive,
    promotionDelete } = require("../controllers/promotion.controller");

const router = Router();

router.get('/public', promotionGetPublic);

router.get('/public/:id', [
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPromotionForId),
    validateFields
], promotionShowPublic);

router.get('/', [
    validateJWT
] , promotionGet);

router.post('/', [
    check('title', 'El titulo es obligatorio').not().isEmpty(),
    fileValidate,
    validateFields
], promotionPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPromotionForId),
    validateFields
], promotionPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPromotionForId),
    validateFields
], promotionShow);

router.get('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPromotionForId),
    validateFields
], promotionActive);

router.get('/des-active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPromotionForId),
    validateFields
], promotionDesActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existPromotionForId),
    validateFields
], promotionDelete);

module.exports = router;
