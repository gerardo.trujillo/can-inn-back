const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existNewsForId, existNewsForSlug } = require("../helpers/db-validators");
const { newsGetPublic, newsGet, newsShowPublic, newsPost, newsPut, newsShow, newsActive, newsDesActive, newsDelete } = require("../controllers/news.controller");

const router = Router();

router.get('/public', newsGetPublic);

router.get('/public/:slug', [
    check('slug', 'No es un id valido').not().isEmpty(),
    check('slug').custom(existNewsForSlug),
    validateFields
], newsShowPublic);

router.get('/', [
    validateJWT
] , newsGet);

router.post('/', [
    check('title', 'El titulo es obligatorio').not().isEmpty(),
    validateFields
], newsPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existNewsForId),
    validateFields
], newsPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existNewsForId),
    validateFields
], newsShow);

router.get('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existNewsForId),
    validateFields
], newsActive);

router.get('/des-active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existNewsForId),
    validateFields
], newsDesActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existNewsForId),
    validateFields
], newsDelete);

module.exports = router;
