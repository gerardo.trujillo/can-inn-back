const {request, response} = require("express");
const { fileUpload: fileUploadHelper } = require("../helpers");
const { News, User, Promotion } = require('../models');
const path = require("path");
const fs = require("fs");


const fileUpload = async (req = request, res = response) => {

    try{
        // const name = await fileUploadHelper(req.files, ['txt', 'md'], 'textos');
        const name = await fileUploadHelper(req.files, undefined, 'news');
        res.json({
            name
        });
    } catch (msg) {
        res.status(400).json({
            msg
        });
    }
}

const fileUploadGallery = async (req = request, res = response) => {
    const { id } = req.params;
    try{
        const name = await fileUploadHelper(req.files, undefined, 'news');
        const news = await News.findById(id);
        const images = news.images;
        images.push(name);
        news.images = images;
        await news.save();
        res.json({
            news
        });
    } catch (msg) {
        res.status(400).json({
            msg
        });
    }
}

const updatedFile = async (req= request, res=response) =>{
    const { id, collection } = req.params;
    let model;
    switch (collection){
        case 'users':
            model = await User.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe un usuario con el id ${id}`
                });
            }
            break;
        case 'news':
            model = await News.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe una noticia con el id ${id}`
                });
            }
            break;
        case 'promotions':
            model = await Promotion.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe una promoción con el id ${id}`
                });
            }
            break;
        default:
            return res.status(500).json({msg: 'Se me olvido Validar esto'});
    }

    // Limpiar imagenes previas
    try{
        if(model.image){
            // Borrar imagen servidor
            const pathImage = path.join(__dirname, '../uploads/', collection, model.image);
            if(fs.existsSync(pathImage)){
                fs.unlinkSync(pathImage);
            }
        }
    } catch (e) {

    }


    const name = await fileUploadHelper(req.files, undefined, collection);
    model.image = name;
    await model.save();

    res.json({
        model
    });

}

const imageShow = async (req= request, res= response) => {
    const { collection, id } = req.params;
    let model;
    switch (collection){
        case 'users':
            model = await User.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe un usuario con el id ${id}`
                });
            }
            break;
        case 'news':
            model = await News.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe una noticia con el id ${id}`
                });
            }
            break;
        case 'promotions':
            model = await Promotion.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe una promoción con el id ${id}`
                });
            }
            break;
        default:
            return res.status(500).json({msg: 'Se me olvido Validar esto'});
    }

    try{
        if(model.image){
            const pathImage = path.join(__dirname, '../uploads/', collection, model.image);
            if(fs.existsSync(pathImage)){
                return res.sendFile(pathImage);
            }
        }
    } catch (e) {

    }
    const pathPlaceholder = path.join(__dirname, '../assets/images/no-image.jpg');
    res.sendFile(pathPlaceholder);
}

const imageShowGallery = async (req= request, res= response) => {
    const { collection, id, img } = req.params;
    let model;
    switch (collection){
        case 'users':
            model = await User.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe un usuario con el id ${id}`
                });
            }
            break;
        case 'news':
            model = await News.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe una noticia con el id ${id}`
                });
            }
            break;
        default:
            return res.status(500).json({msg: 'Se me olvido Validar esto'});
    }

    try{
        if(model.images.length > 0){
            const pathImage = path.join(__dirname, '../uploads/', collection, img);
            if(fs.existsSync(pathImage)){
                return res.sendFile(pathImage);
            }
        }
    } catch (e) {

    }
    const pathPlaceholder = path.join(__dirname, '../assets/images/no-image.jpg');
    res.sendFile(pathPlaceholder);
}

const deletedFile = async (req= request, res=response) =>{
    const { id, collection, img } = req.params;
    let model;
    switch (collection){
        case 'users':
            model = await User.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe un usuario con el id ${id}`
                });
            }
            break;
        case 'news':
            model = await News.findById(id);
            if (!model){
                return  res.status(400).json({
                    msg: `No existe una noticia con el id ${id}`
                });
            }
            break;
        default:
            return res.status(500).json({msg: 'Se me olvido Validar esto'});
    }

    // Limpiar imagenes previas
    try{
        if(model.images.length > 0){
            // Borrar imagen servidor
            const pathImage = path.join(__dirname, '../uploads/', collection, img);
            if(fs.existsSync(pathImage)){
                fs.unlinkSync(pathImage);
            }

        }
    } catch (e) {

    }
    const images = model.images;
    const index = images.findIndex(item => item == img);
    images.splice(index, 1);
    model.images = images;
    await model.save();

    res.json({
        model
    });

}

module.exports = {
    fileUpload,
    updatedFile,
    fileUploadGallery,
    imageShowGallery,
    deletedFile,
    imageShow
}
