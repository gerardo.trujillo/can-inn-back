const { request, response } = require('express');
const User = require('../models/user.model');
const bcryptjs = require('bcryptjs');


const userGet = async (req, res = response) => {

    const [ total, users ] = await Promise.all([
        User.countDocuments({delete:false}),
        User.find({delete:false})
    ]);

    res.json({
        total,
        users
    });
};

const userPost = async (req = request, res = response) => {

    const { name, email, password, role } = req.body;
    const user = new User({name, email, password, role});

    // Encriptar contraseñana
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);

    // Guardar en Base de datos
    await user.save();

    res.json({
        user
    })
}

const userActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [user] = await Promise.all([
        User.findByIdAndUpdate(id,{active:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        user,
        authenticatedUser
    });
}

const userDesActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [user] = await Promise.all([
        User.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        user,
        authenticatedUser
    });
}

const userShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const user = await User.findById(id);
    res.status(200).json({
        user
    });
}

const userPut = async ( req = request, res = response) => {
    const id = req.params.id;
    const { _id, password, google, role, ...rest } = req.body;

    //  validar Contra base de datos
    if(password){
        // Encriptar contraseñana
        const salt = bcryptjs.genSaltSync();
        rest.password = bcryptjs.hashSync(password, salt);
    }

    const user = await User.findByIdAndUpdate(id, rest, {new: true});
    res.status(201).json({
        user
    });
}

const userDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [user] = await Promise.all([
        User.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        user,
        authenticatedUser
    });
}

module.exports = {
    userGet,
    userPost,
    userActive,
    userDesActive,
    userShow,
    userPut,
    userDelete
}
