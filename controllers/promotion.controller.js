const { request, response } = require('express');
const Promotion = require('../models/promotion.model');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

const promotionGetPublic = async (req, res = response) => {

    const { limit = 5, page = 1 } = req.query;
    let from = (page - 1) * limit;

    const [ total, promotions ] = await Promise.all([
        Promotion.countDocuments({delete:false}),
        Promotion.find({delete:false})
            .limit(Number(limit))
            .skip(Number(from))
    ]);

    res.json({
        total,
        promotions
    });
};

const promotionGet = async (req, res = response) => {

    const [ total, promotions ] = await Promise.all([
        Promotion.countDocuments({delete:false}),
        Promotion.find({delete:false})
    ]);

    res.json({
        total,
        promotions
    });
};

const promotionPost = async (req = request, res = response) => {

    const { title, name, description, url } = req.body;
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'promotions');
    }
    const promotion = new Promotion({title, name, description, image, url});



    // Guardar en Base de datos
    await promotion.save();

    res.json({
        promotion
    })
}

const promotionActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [promotion] = await Promise.all([
        Promotion.findByIdAndUpdate(id,{active:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        promotion,
        authenticatedUser
    });
}

const promotionDesActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [promotion] = await Promise.all([
        Promotion.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        promotion,
        authenticatedUser
    });
}

const promotionShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const promotion = await Promotion.findById(id);
    res.status(200).json({
        promotion
    });
}

const promotionShowPublic = async ( req = request, res = response) => {
    const id = req.params.id;

    const promotion = await Promotion.findById(id);
    res.status(200).json({
        promotion
    });
}

const promotionPut = async ( req = request, res = response) => {
    const id = req.params.id;
    const { title, name, description, url } = req.body;
    const data = {
        title,
        name,
        description,
        url
    }

    const promotion = await Promotion.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        promotion
    });
}

const promotionDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [promotion] = await Promise.all([
        Promotion.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        promotion,
        authenticatedUser
    });
}

module.exports = {
    promotionGetPublic,
    promotionShowPublic,
    promotionGet,
    promotionPost,
    promotionActive,
    promotionDesActive,
    promotionShow,
    promotionPut,
    promotionDelete
}
