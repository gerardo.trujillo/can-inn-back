const { request, response } = require('express');
const { News, Promotion} = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

const newsGetPublic = async (req, res = response) => {

    const { limit = 5, page = 1 } = req.query;
    let from = (page - 1) * limit;

    const [ totalNews, news, totalpromotions, promotions ] = await Promise.all([
        News.countDocuments({delete:false}),
        News.find({delete:false})
            .limit(Number(limit))
            .skip(Number(from)),
        Promotion.countDocuments({delete:false}),
        Promotion.find({delete:false})
            .limit(Number(limit))
            .skip(Number(from)),
    ]);

    res.json({
        totalNews,
        news,
        totalpromotions,
        promotions
    });
};

const newsGet = async (req, res = response) => {

    const [ total, news ] = await Promise.all([
        News.countDocuments({delete:false}),
        News.find({delete:false})
    ]);

    res.json({
        total,
        news
    });
};

const newsPost = async (req = request, res = response) => {

    const { title, content, intro, date } = req.body;
    const slug = urlSlug(title);
    const existNews = await News.findOne({slug});
    if (existNews){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el titulo de la noticia`
        })
    }
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'news');
    }
    const news = new News({title, slug, content, intro, date, image});



    // Guardar en Base de datos
    await news.save();

    res.json({
        news
    })
}

const newsActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [news] = await Promise.all([
        News.findByIdAndUpdate(id,{active:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        news,
        authenticatedUser
    });
}

const newsDesActive = async (req = request, res = response) => {

    const id = req.params.id;

    const [news] = await Promise.all([
        News.findByIdAndUpdate(id,{active:false}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        news,
        authenticatedUser
    });
}

const newsShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const news = await News.findById(id);
    res.status(200).json({
        news
    });
}

const newsShowPublic = async ( req = request, res = response) => {
    const slug = req.params.slug;

    const news = await News.findOne({slug});
    res.status(200).json({
        news
    });
}

const newsPut = async ( req = request, res = response) => {
    const id = req.params.id;
    const { title, content, intro, date } = req.body;
    const data = {
        title,
        slug: urlSlug(title),
        content,
        intro,
        date
    }

    const news = await News.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        news
    });
}

const newsDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [news] = await Promise.all([
        News.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        news,
        authenticatedUser
    });
}

module.exports = {
    newsGetPublic,
    newsShowPublic,
    newsGet,
    newsPost,
    newsActive,
    newsDesActive,
    newsShow,
    newsPut,
    newsDelete
}
