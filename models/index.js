const News = require('./news.model');
const Promotion = require('./promotion.model');
const User = require('./user.model');
const Server = require('./server');

module.exports = {
    News,
    Promotion,
    User,
    Server
}
